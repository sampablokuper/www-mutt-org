<!DOCTYPE html>
<html>
  <head>
    <title>Mutt 1.11 Release Notes</title>
  </head>

  <body>
    <h1>Mutt 1.11 Release Notes</h1>
    <p>
      There were lots of little bug fixes and some internal changes
      during this development cycle.  I think the most exciting
      changes were IMAP QRESYNC support and inotify monitoring, but
      there are a few other interesting new features too.
    </p>


    <h2>IMAP CONDSTORE and QRESYNC Support</h2>
    <p>
      Opening a large header-cached IMAP mailbox in Mutt is slow for
      two reasons.  First, Mutt needs to know the current flags for
      messages.  Second, Mutt needs to know which messages (still)
      exist in the mailbox.
    </p>
    <p>
      Although it provides other functionality, Mutt uses the
      CONDSTORE extension to obtain flag updates since the last time
      the mailbox was opened.  Mutt still needs to query the list of
      all messages, but it skip asking for flags at the same time.
      This makes the query slightly faster.  The flag updates are then
      retrieved in a separate, usually much smaller, query.
    </p>
    <p>
      The QRESYNC extension solves the second problem.  It provides a
      way to find out what messages still exist without querying the
      whole list.  This enables much faster opening of large
      mailboxes.  Only changes since the last time need to be
      downloaded.
    </p>
    <p>
      This feature is still new, and so defaults off.  If you don't
      use Gmail, we would love to hear how (or if) the feature works
      for you.  Please try it out by
      enabling <a href="/doc/manual/#imap-condstore">$imap_condstore</a>
      and <a href="/doc/manual/#imap-qresync">$imap_qresync</a>.
    </p>
    <p>
      (Unfortunately Gmail doesn't support QRESYNC, and its CONDSTORE
      implementation slows things down noticably, removing all benefit
      of enabling the extension.)
    </p>

    <h2>Inotify Mailbox Monitoring</h2>
    <p>
      This is enabled by default for Linux users, but can be disabled
      at compile-time with the --disable-filemonitor flag.  It uses
      inotify to speed up new mail notification for local mailboxes.
    </p>
    <p>
      This feature is in addition to the normal polling via
      <a href="/doc/manual/#timeout">$timeout</a> and
      <a href="/doc/manual/#mail-check">$mail_check</a>.  It should
      provide faster notifications without having to turn those values
      way down.
    </p>


    <h2>OAUTHBEARER Support</h2>
    <p>
      This has been implemented and tested against Gmail.  Mutt uses
      external scripts to generate the refresh token.  See
      <a href="/doc/manual/#oauth">OAUTHBEARER Support</a>.
    </p>
    <p>
      To enable, "oauthbearer" needs to be added to the appropriate authenticator list:
      <a href="/doc/manual/#imap-authenticators">$imap_authenticators</a>,
      <a href="/doc/manual/#smtp-authenticators">$smtp_authenticators</a>, or
      <a href="/doc/manual/#pop-authenticators">$pop_authenticators</a>.  Then the
      token refresh script needs to be set up in
      <a href="/doc/manual/#imap-oauth-refresh-command">$imap_oauth_refresh_command</a>,
      <a href="/doc/manual/#smtp-oauth-refresh-command">$smtp_oauth_refresh_command</a>, or
      <a href="/doc/manual/#pop-oauth-refresh-command">$pop_oauth_refresh_command</a>.
    </p>
    <p>
      Personally, I haven't used this, although it was contributed by
      Brandon Long, who worked on much of the initial IMAP
      implementation for Mutt.  As always, feedback is appreciated.
    </p>


    <h2>Dynamic $index_format Content</h2>
    <p>
      Two new features: the %@name@ <a href="/doc/manual/#index-format">$index_format</a>
      expando, and <a href="/doc/manual/#index-format-hook">index-format-hook</a>
      combine to allow dynamic content in the index.
    </p>
    <p>
      %@name@ specifies a placeholder in the
      <a href="/doc/manual/#index-format">$index_format</a>.  The current message will
      be pattern matched against
      <a href="/doc/manual/#index-format-hook">index-format-hook</a>s with the same name.
      The first matching hook's format-string will be substituted and evaluated.
    </p>
    <p>
      The format-string in an index-format-hook can contain other
      expandos, including a %@name% expando.  This allows a fair amount of flexibility, while
      still keeping the $index_format readable.
    </p>
    <p>
      One potential use is dynamically formatting dates based on the age of the message, for
      example:
    </p>
    <pre>
      set index_format="%Z %-20.20F %s %*  %@reldate@"

      index-format-hook  reldate  "%d&lt;1d"  "%[!%X]"   # show time
      index-format-hook  reldate  "~A"     "%[!%x]"   # show date
    </pre>
    <p>
      In addition, to allow for more granular matching, the
      <a href="/doc/manual/#date-patterns">relative date units</a> H
      (hour), M (minute), and S (second) have been added.
    </p>


    <h2>Smaller Features and Noteworthy Bugfixes:</h2>

    <h3>Manually updating mailbox stats</h3>
    <p>
      When <a href="/doc/manual/#mail-check-stats">$mail_check_stats</a> is set,
      Mutt will periodically check the unread, flagged, and total message counts
      for monitored <a href="/doc/manual/#mailboxes">mailboxes</a>.
    </p>
    <p>
      If you'd rather perform this update yourself, the new function &lt;check-stats&gt;
      can be invoked.
    </p>

    <h3>Thread limited views and new mail</h3>
    <p>
      Thread limited views, e.g. ~(~f me), did not properly display
      new messages that arrived while the limit was in effect.  This
      has now been fixed, along with a few other related issues.
    </p>

    <h3>-z and -Z work for IMAP mailboxes</h3>
    <p>
      Command line argument -z, checks
      <a href="/doc/manual/#spoolfile">$spoolfile</a> or the -f argument
      for new mail and exits if there is none.  It previously was not
      hooked up to poll an IMAP mailbox but now does.
    </p>
    <p>
      -Z polls all monitored mailboxes.  It previously worked with
      IMAP only if
      <a href="/doc/manual/#imap-passive">$imap_passive</a> was unset.
      It will now poll IMAP mailboxes, regardless of the value of
      $imap_passive.
    </p>

    <h3>$abort_noattach skips quoted lines</h3>
    <p>
      <a href="/doc/manual/#abort-noattach">$abort_noattach</a> will
      now skip scanning lines that are considered "quoted lines", as
      determined
      by <a href="/doc/manual/#quote-regexp">$quote_regexp</a> and
      <a href="/doc/manual/#smileys">$smileys</a>.  This seems
      reasonable behavior, so I have not included an option to toggle
      the behavior.
    </p>

    <h3>Ability to abort opening a large IMAP mailbox</h3>
    <p>
      Mutt now allows ctrl-c to abort opening an IMAP mailbox.  Note
      this will close the mailbox and connection.
    </p>

    <h3>Composing to the current message sender</h3>
    <p>
      The &lt;compose-to-sender&gt; function allows you to compose
      a <b>new</b> email to the sender of the currently selected
      message(s).  Note this generates a brand new email: it doesn't
      respect Reply-To, Mail-Followup-To, or generate a References header.
    </p>
    <p>
      This feature was requested in
      <a href="https://gitlab.com/muttmua/mutt/issues/63">#63</a>
      based on a NeoMutt feature.  However the implementation is
      different (well... better ;-) ), with proper mode checks, and
      works on messages in the attachment menu too.
    </p>

    <h3>Address book queries and multibyte characters</h3>
    <p>
      Strangely, the <a href="/doc/manual/#query">query menu</a> did
      not properly handle multibyte characters, resulting in
      misaligned output for those cases.  It's a small but annoying
      thing fixed in this release.
    </p>

    <h3>pgpring renamed to mutt_pgpring</h3>
    <p>
      Although it's unlikely there are many original PGP (Pretty Good
      Privacy) users, the binary to list the keyring has been renamed
      from pgpring to mutt_pgpring (due to a
      <a href="https://gitlab.com/muttmua/mutt/issues/71">naming
      conflict</a> with the signing-party package).  Users will need to
      update their
      <a href="/doc/manual/#pgp-list-pubring-command">$pgp_list_pubring_command</a>
      and
      <a href="/doc/manual/#pgp-list-secring-command">$pgp_list_secring_command</a>
      to refer to mutt_pgpring instead.  See the contrib/pgp5.rc or
      contrib/pgp6.rc files.
    </p>

    <h3>SHA-256 in cert prompts</h3>
    <p>
      MD5 fingerprints were removed and replaced with SHA-256 fingerprints.
    </p>

    <h3>Non-threaded $sort_aux</h3>
    <p>
      For a <a href="/doc/manual/#sort">$sort</a> value other
      than "threads", <a href="/doc/manual/#sort-aux">$sort_aux</a>
      "reverse-" prefixed values are now functional.
    </p>

    <h3>GNU Info manual</h3>
    <p>
      For those interested, the manual can now be generated in GNU
      Info format too.  Install-info is required to properly install
      the info file.  Those compiling from git will also need
      docbook2x-texi and makeinfo installed.
    </p>
  </body>
</html>
